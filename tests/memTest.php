<?php

use PHPUnit\Framework\TestCase;

class memTest extends TestCase {

    public function testAlloc(){
        // create buffer
        $bf= new MemoryManager(10);

        // allocate 5 spots, index of aray is 0 
        $this->assertEquals( $bf->alloc(5), 0);
        // allocate 5 spots, index of aray is 5
        $this->assertEquals( $bf->alloc(4), 5);
        // allocate 5 spots, index of aray is -1 as there is no space left to allocate
        $this->assertEquals( $bf->alloc(5), -1);
        // free space allocated in 5th spot
        $bf->free(5);
        // allocate 2 spots, index of aray is 5 as the 5th spot was freed in prev operation.
        $this->assertEquals( $bf->alloc(2), 5);
        // allocate 2 spots, index of aray is 5 as the 7th.
        $this->assertEquals( $bf->alloc(2), 7);
        // set memory from index 7, indexes 7 and 8 will be occupied wth '7'
        $bf->set_mem(7, [7,7]);
        print_r($bf);
    }
}