<?php


// simple allocator
// it uses a list of blocks to keep track of allocated spaces
// the algorthm is really simple, it justs look for an empty space and if it finds it it returns the index
// it doesnt compacts the empty space after freeing a block, so after ussage the blocks grows fragmented
// the list of block is just an array, so there is probably a better way to structure the data,
// it also doesnt do any error checking so overflows (overwriting to adjacents blocks) can happen.
class MemoryManager {


    // allocate buffer and create a block that is free
    function __construct($len){
        $this->buffer= array_fill(0, $len, 0);
        $this->blocks= [['index'=> 0, 'len' => $len, 'is_alloc' => 0]];
    }

    protected $buffer= [];
    // this keeps track of the allocated blocks
    protected $blocks= [];


    // allocate a block, takes as  parameter the length of the block to allocate
    // checks for empty blocks that are longer o same length of the requested alloc
    // if successfull returns the index of the buffer
    // else it returns -1
    public function alloc($len) {

        for ($i=0; $i < count($this->blocks); $i++) { 
            if ((!$this->blocks[$i]['is_alloc']) && ($this->blocks[$i]['len'] >= $len )) {
                if($this->blocks[$i]['len'] == $len){
                    $this->blocks[$i]['is_alloc'] = 1;
                    return $this->blocks[$i]['index'];
                } else {
                    $this->blocks[]= [
                        'index'=> $this->blocks[$i]['index'] + $len , 
                        'len' =>  $this->blocks[$i]['len'] - $len, 
                        'is_alloc' => 0
                    ];

                    $this->blocks[$i]['len'] = $len;
                    $this->blocks[$i]['is_alloc'] = 1;

                    return $this->blocks[$i]['index'];
                }
            } 
        }

        return -1;
    }

    // marks a block as free if occuped
    // it takes as parameter the index of the buffer (wich would be returned by the alloc function)
    public function free($idx) {
        for ($i=0; $i < count($this->blocks); $i++) { 
            if( $this->blocks[$i]['index'] == $idx){
                $this->blocks[$i]['is_alloc'] = 0;
            }
        }
    }

    // this function allows to write to the buffer using an index
    public function set_mem($idx, $mem) {
        for ($i=0; $i < count($mem); $i++) { 
            $this->buffer[$idx+$i]= $mem[$i];
        }
    }


}